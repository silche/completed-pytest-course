from scripts.chp2_tdd_pytest.video3_exceptions.mapmaker_exceptions import Point
import pytest


def test_make_one_point():
    p1 = Point("Oslo", 59.9139, 10.7523)
    assert p1.get_lat_long() == (59.9139, 10.7523)


def test_invalid_point_generation():  # TO DO
    with pytest.raises(ValueError) as exp:
        Point("Ottawa", 45.42472, -755.69500)
    assert str(exp.value) == 'Invalid [latitude, longitude] combination.'
