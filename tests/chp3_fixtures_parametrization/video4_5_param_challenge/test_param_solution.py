from io import StringIO
import os
import pytest

from scripts import data_processor, data_aggregator


@pytest.fixture(scope="module")
def city_list_location():
    return 'tests/resources/cities/'


@pytest.fixture(scope="module")
def process_data(city_list_location):
    files = os.listdir(city_list_location)

    def _specify_type(filename_or_type):
        for f in files:
            if filename_or_type in f:
                if filename_or_type == '.json':
                    data = data_processor.json_reader(city_list_location + f)
                else:
                    data = data_processor.csv_reader(city_list_location + f)
        return data

    yield _specify_type


@pytest.mark.parametrize("country,stat,expected_alt", [
    ('Canada', 'Mean', 246.58),
    ('Canada', 'Median', 154.0),
    ('Germany', 'Median', 100.6),
    ])
def test_csv_writer(process_data, country, stat, expected_alt):
    """
    TO DO Task: Update the function to be parametrized with 3 scenarios
    """
    data = process_data(filename_or_type="clean_map.csv")
    andorran_median_res = data_aggregator.atitude_stat_per_country(data, country, stat)
    output_location = StringIO()
    data_aggregator.csv_writer(andorran_median_res, output_location)

    result = output_location.getvalue().strip('\r\n')
    assert result == f'Country,{stat}\r\n{country},{expected_alt}'
